<?php
class Config{

private $configfile;
public  function __construct(){	
	
	$this->configfile=parse_ini_file('config.ini',true);
	if($this->configfile['error-reporting']['debug']=="on"){

		
		ini_set('display_errors','On');
	}else{
	ini_set('display_errors','Off');
	}

}
public static function load($class){
include(__DIR__ . "/" . "../classes/class." .strtolower($class). ".php");
}
public function getDatabaseInfo(){
return $this->configfile['database'];
}

}
$test=new Config();
spl_autoload_register('config::load');
