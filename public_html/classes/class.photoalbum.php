<?php
class photoAlbumException extends Exception{

}
class noAlbumWithThatIdException extends photoAlbumException{

}
class PhotoAlbum{

	private $userFolder;

	private $userAlbumId;

	private $uploadfolder;

	public function __construct($userId){
		$this->userAlbumId =(INT) $this->getUserFolder((INT)$userId)["albumid"];
		$this->uploadfolder = __DIR__."/../uploads/"."/".$this->userAlbumId;
	}

	private function getUserFolder($userId){
		$album = new user;

		$sql = "call getUserAlbum(?)";
		
		$params =[$userId];
		$album = $album->getUserAlbumFolder($sql,$params);
		if(!empty($album)){

			return $album;

		}else{

			throw new noAlbumWithThatIdException("No photoAlbum found with that userid");

			

		}
	}

	public function createNewUserAlbum($foldername){

		if($this->exist($this->uploadfolder) === false){
			
			$this->createUserFolder();
					

		}
		else if($this->exist($this->uploadfolder."/".$foldername) === false){

				mkdir($this->uploadfolder."/".$foldername);

			}

		return true;
	}
	public function deleteAlbum($folder){
		if($this->exist($this->uploadfolder."/".$folder) === true){

			rmdir($this->uploadfolder."/".$folder);
		}

		return;
	}


	private function createUserFolder(){
		mkdir($this->uploadfolder."/");
	}

	public function listAllUserAlbums(){

		$dir = scandir($this->uploadfolder."/");

		$result = [];

		foreach($dir as $value ){
			
			switch ($value) {

				case ".":
				
					break;

				case "..":

					break;
			
				default:
				
					$result[]=$value;
			
				break;	

			}


		}

		return $result;
	}

	private function exist($folder){
		if(file_exists($folder) === true){

			return true;
		}

		return false;
	}
}